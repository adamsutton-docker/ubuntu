ROOT_DIR			?= $(realpath $(dir $(firstword $(MAKEFILE_LIST))))
BUILD_ROOT			?= $(ROOT_DIR)

DOCKER_IMAGE		?= $(notdir $(ROOT_DIR))
DOCKER_HASH			?= $(shell git rev-parse HEAD)
DOCKER_USER         ?= docker-user
UID					?= $(shell id -u)
DOCKER_BUILD_ARGS	?=

IN_DOCKER		    ?= $(shell cat /proc/1/cgroup | grep -q docker && echo yes)
define docker
	$(if $(IN_DOCKER), $(2), docker run -it -v $(ROOT_DIR):/project -w /project $(1) $(MAKE) -$(MAKEFLAGS) $@)
endef

build-docker:
	docker build \
        --pull \
        ${DOCKER_BUILD_ARGS} \
        -t $(DOCKER_IMAGE):$(DOCKER_HASH).WIP \
		-f $(ROOT_DIR)/Dockerfile \
		context

.PHONY: verify

verify:
	docker run \
		-i \
		-u $(DOCKER_USER) \
		-v $(BUILD_ROOT):/project \
		-w /project \
		$(DOCKER_IMAGE):$(DOCKER_HASH).WIP \
		bash -c "make -C verify"

tag:
	docker tag \
		${DOCKER_IMAGE}:${DOCKER_HASH}.WIP ${DOCKER_IMAGE}:${DOCKER_HASH}
	docker tag \
		${DOCKER_IMAGE}:${DOCKER_HASH}.WIP ${DOCKER_IMAGE}:latest

clean:
	make -C verify clean
