# Docker Base Image - Ubuntu 20.04

## Introduction

Docker container based on Ubuntu 20.04 that provides some very basic customisations
that I tend to use in most of my downstream containers.
