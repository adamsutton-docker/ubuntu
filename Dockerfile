#
# My personal base docker container that adds some basic packages and
# customisations to the latest Ubuntu LTS.
#

FROM ubuntu:20.04

MAINTAINER dev@adamsutton.me.uk

#
# Global config
#

ARG DEBIAN_FRONTEND=noninteractive
COPY /apt.conf.d/* /etc/apt/apt.conf.d/

#
# Define local user
#

RUN set -xe; \
    addgroup \
        --gid 1000 \
        docker-user \
        ; \
    adduser \
        --uid 1000 \
        --ingroup docker-user \
        --shell /bin/bash \
        --home /mnt/home \
        --disabled-password \
        --gecos "Docker User" \
        docker-user \
        ;

#
# Add packages
#

# Basic packages
RUN set -xe; \
    apt-get update -q \
        ; \
    apt-get install -q -y --no-install-recommends \
        curl \
        htop \
        iputils-ping \
        make \
        screen \
        vim \
        ;

# Python (just because)
RUN set -xe; \
    apt-get update -q \
        ; \
    apt-get install -q -y --no-install-recommends \
        python3-minimal \
        python3-pip \
        ; \
    pip3 install -U \
        pip \
        ; \
    update-alternatives --install /usr/bin/python python /usr/bin/python3 10 \
        ;
